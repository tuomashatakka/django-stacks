from django.db.models import CharField
from stacks.models import BaseStackingModel


class Page(BaseStackingModel):
    title = CharField(default="", max_length=127)
