from django.views.generic import TemplateView
from stacks.views import BaseStackUpdateView
from .models import Page


class StackExampleView(BaseStackUpdateView):
    template_name = 'default.html'
    model = Page
