from django.apps import AppConfig


class Application(AppConfig):
    name = 'examples'
