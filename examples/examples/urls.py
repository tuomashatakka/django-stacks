from django.conf.urls import patterns, include, url
from examples.views import StackExampleView


urlpatterns = [
    url(r'^$', StackExampleView.as_view(), name='home'),
]
