from django.db.models import (
    Model, CharField, TextField, ForeignKey, OneToOneField,
    PositiveSmallIntegerField, CASCADE, )
from django.conf import settings
from django.utils import html


class BaseStackingModel(Model):
    stack = OneToOneField("Stack", on_delete=CASCADE,
                           related_name='parent')


    @classmethod
    def create(cls, **kw):
        kw.update({
            'stack': Stack.objects.create()
        })
        mdl = cls(**kw)
        return mdl

    @property
    def content(self):
        if self.stack is None:
            return ""
        return html.mark_safe(self.stack.parse())

    @property
    def blocks(self):
        if self.stack is None:
            return []
        return self.stack.blocks.all()

    def __str__(self):
        return self.content


class Stack(Model):
    position = PositiveSmallIntegerField(blank=True, null=True)

    def parse(self, **options):
        content = ""
        for block in self.blocks.all():
            content = html.mark_safe("{}\n{}".format(
                content, block.parse(**options)))
        return html.mark_safe(content)

    def add_section(self, **kw):
        kw.update({
            'parent': self,
            'position': len(self.blocks.all())})
        sect = Section.objects.create(**kw)
        return sect

    class Meta:
        verbose_name = "stack"
        ordering = ['position', ]


class Section(Model):
    parent = ForeignKey(Stack, on_delete=CASCADE, related_name='blocks')
    name = CharField(max_length=63, blank=True, null=True)
    content = CharField(max_length=63, blank=True, null=True)
    position = PositiveSmallIntegerField(blank=False)

    def parse(self, **options):
        content = ""
        for attribute in self.attributes.all():
            content = html.mark_safe("{}\n{}\n{}".format(
                content, self.content, attribute.parse(**options)))
        return html.mark_safe(content)

    def __str__(self):
        return self.parse()

    class Meta:
        verbose_name = 'stack section'
        ordering = ['parent', 'position', ]
        index_together = ["parent", "position"]
        # unique_together = ("parent", "position")


class Attributes(Model):
    parent = ForeignKey(Section, on_delete=CASCADE, related_name='attributes')
    key = CharField(max_length=63, blank=True, null=True)
    value = CharField(max_length=63, blank=True, null=True)

    def parse(self, **options):
        return str(self)

    def __str__(self):
        return "{p}.{k}: {v}".format(k=self.key, v=self.value, p=self.parent.name)

    class Meta:
        verbose_name = 'stack section attribute'
        ordering = ['parent', 'key', ]
        index_together = ['parent', 'key']
