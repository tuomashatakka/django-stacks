from django.conf import settings
from django.http import JsonResponse
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from stacks.models import Stack


app_label = 'stacks'
ERROR_CODES = {300: 'Forbidden',
               404: 'Not found'}


class BaseStackUpdateView(DetailView):
    template_name = 'default.html'
    stack = None
    action = None
    model = None

    def create(self, **attrs):
        """
        If the queryset is empty, creates a new page and a stack for it.
        Otherwise creates a stack for the queried page.
        """
        page = self.get_object()
        stack = None

        if page is not None:
            return page
            attrs.update({'parent': page})
            stack = Stack.objects.create(**attrs)

        if page is None and self.model is not None:
            page = self.model.objects.create(**attrs)

        return page

    def get_object(self, *args, **kwargs):
        try:
            instance = super(BaseStackUpdateView,
                             self).get_object(*args, **kwargs)
        except AttributeError:
            try:
                instance = self.model.objects.get()
            except Exception:
                instance = None
        return instance

    def get_stack(self, **filter):
        parent = filter.get('parent') or self.get_object()

        try:
            filter['parent'] = parent
            self.stack = Stack.objects.get(**filter)
        except Exception:
            self.stack = None
        return self.stack

    def _do_action(self, action, *args, **kwargs):
        response = {
            'action': action,
        }
        if action == 'create':
            page = self.create(**kwargs)
            response.update({
                'instance': {
                    'id': page.pk,
                    'model': self.model.__class__.__name__,
                },
                'stack': {
                    'id': page.stack.pk,
                    'model': "stacks.models.Stack",
                }
            })
        return response or self._do_error(300)

    def _do_error(self, code, *args, **kwargs):
        return {'code': code,
                'message': ERROR_CODES[code] or "",
                'callback': None }

    def dispatch(self, request, *args, **kwargs):
        response = super(BaseStackUpdateView, self).dispatch(request, *args, **kwargs)
        self.object = self.get_object()
        action = kwargs.get('action')

        if 'action' not in kwargs:
            return response
        return JsonResponse(self._do_action(action, *args, **kwargs))
