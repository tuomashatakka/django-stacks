from django.apps import AppConfig


class Application(AppConfig):
    name = 'stack'
    verbose_name = "stack block"

    def ready(self):
        import stacks.views
